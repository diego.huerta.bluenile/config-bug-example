package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@AllArgsConstructor
public class Message {
    private final transient SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    Date date;
    @Getter @Setter
    User author;
    @Getter @Setter
    User recipient;
    @Getter @Setter
    String message;

    public String getDate() {
        return formatter.format(date);
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setDate(String date) throws ParseException {
        this.date = formatter.parse(date);
    }
}
