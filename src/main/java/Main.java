import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Message;
import models.User;

import java.util.Date;
import java.util.Scanner;

public class Main {
    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Type your ID");
        String authorId = scanner.next();
        System.out.println("Type your username");
        String authorUsername = scanner.next();
        System.out.println("Type the recipient id");
        String recipientId = scanner.next();
        System.out.println("Type the recipient username");
        String recipientUsername = scanner.next();
        System.out.println("Type the message");
        String messageString = scanner.next();

        User author = new User(authorId, authorUsername);
        User recipient = new User(recipientId, recipientUsername);
        Message message = new Message(new Date(), author, recipient, messageString);

        System.out.println("\n\n\nThe formatted message is:\n");
        System.out.println(gson.toJson(message));
    }
}
